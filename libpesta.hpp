#ifndef LIBPESTA_HPP_INCLUDED
#define LIBPESTA_HPP_INCLUDED

#include "graphics.hpp"
#include <cmath>
#include <set>

#define COLOR_WHITE genv::color(255, 255, 255)
#define COLOR_BLACK genv::color(0, 0, 0)
#define COLOR_RED genv::color(255, 0, 0)
#define COLOR_GREEN genv::color(0, 255, 0)
#define COLOR_BLUE genv::color(0, 0, 255)

class URandom
{
	int _n;
	std::set<int> _p;

public:
	URandom(int n);
	int next();
	void reset();
	void skip(int r);
};

struct Font
{
	std::string name;
	int size;

	Font(std::string n, int s) : name(n), size(s) {}
	inline int charWidth(char c) const { return stringWidth(std::string(1, c)); }
	int charHeight() const;
	int stringWidth(std::string s) const;
	std::string shortenText(std::string t, int max_width) const;
};

class mygroutput
{
	int _XX, _YY;
	genv::color bg = COLOR_BLACK;

public:
	void open(int XX, int YY, genv::color background = COLOR_BLACK);

	int getXX() const { return _XX; }
	int getYY() const { return _YY; }
	genv::color getBgColor() const { return bg; }

	void clear();
	inline void load_font(Font f) { genv::gout.load_font(f.name, f.size); }
};

class mycanvas : public genv::canvas
{
protected:
	int _width, _height;

public:
	mycanvas() : canvas() {}
	mycanvas(int w, int h) : canvas(w, h), _width(w), _height(h) {}
	mycanvas(int w, int h, Font f) : canvas(w, h), _width(w), _height(h) { load_font(f.name, f.size); }
	bool loadfont(Font f) { return load_font(f.name, f.size); }
	void open_with_font(int w, int h, Font f) { _width = w; _height = h; open(w, h); loadfont(f); }
	void clear() { *this << genv::move_to(0, 0) << COLOR_BLACK << genv::box(_width, _height); };
	int width() const { return _width; }
	int height() const { return _height; }
};

extern mygroutput mygout;

inline void clear_window() { mygout.clear(); }

struct vect4
{
	double a, b, c, d;
	vect4(double _a, double _b, double _c, double _d) : a(_a), b(_b), c(_c), d(_d) {};
};

struct vect
{
	double x, y;
	vect(double _x, double _y) : x(_x), y(_y) {};

	double distance(const vect& a);

	vect operator+ (const vect& a);
	void operator+= (const vect& a);
	vect operator* (double a);
};

vect operator* (const vect& v, const vect4& m);

struct coord : vect
{
	coord(int _x, int _y) : vect(_x, _y) {}
};

struct polygon
{
	float r;
	int n, s;
	bool fill;
	genv::color fill_color;
	float precision;
	polygon(float _r, int _n, int _s, bool _fill = false, genv::color _fill_color = COLOR_BLACK, float _precision = 0.25) : r(_r), n(_n), s(_s), fill(_fill), fill_color(_fill_color), precision(_precision) {}
	void operator () (genv::canvas& out);
};

struct rectangle
{
	int w, h, s;
	bool fill;
	genv::color fill_color;
	rectangle(int _w, int _h, int _s, bool _fill = false, genv::color _fill_color = COLOR_BLACK) : w(_w), h(_h), s(_s), fill(_fill), fill_color(_fill_color) {}
	void operator () (genv::canvas& out);
};

struct circle
{
	int r, s;
	bool fill;
	genv::color line_color, fill_color;
	circle(int _r, int _s, bool _fill = false, genv::color _line_color = COLOR_WHITE, genv::color _fill_color = COLOR_BLACK) : r(_r), s(_s), fill(_fill), line_color(_line_color), fill_color(_fill_color) {}
	void operator () (genv::canvas& out);
};

struct circle2
{
	int r, s;
	bool fill;
	genv::color fill_color;
	circle2(int _r, int _s, bool _fill = false, genv::color _fill_color = COLOR_BLACK) : r(_r), s(_s), fill(_fill), fill_color(_fill_color) {}
	void operator () (genv::canvas& out);
};

struct triangle
{
	int w, h, s;
	bool fill;
	genv::color line_color, fill_color;
	triangle(int _w, int _h, int _s, bool _fill = false, genv::color _line_color = COLOR_WHITE, genv::color _fill_color = COLOR_BLACK) : w(_w), h(_h), s(_s), fill(_fill), line_color(_line_color), fill_color(_fill_color) {}
	void operator () (genv::canvas& out);
};

#endif // LIBPESTA_HPP_INCLUDED
