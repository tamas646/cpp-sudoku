# CPP Sudoku


## About
This is a basic Sudoku game written in C++ based on the [**ITK_libgraphics**](https://github.com/flugi/ITK_graphicslib) package made by [@flugi.](https://github.com/flugi)

The project created at **Pázmány Péter Catholic University, Faculty of Information Technology and Bionics**.

Example sudoku boards have been generated with [**sudoku.js**](https://github.com/robatron/sudoku.js).


## Compile instructions
There is a precompiled file (compiled for linux) you may need to replace: **libgraphics.a**. For more information visit the [ITK_libgraphics](https://github.com/flugi/ITK_graphicslib) repository.

The graphics package based on the **SDL2** and **SDL2_ttf** lib. Ensure that these packages are available on your system.

For the rest of the compiling process use the makefile.


## How to run the program
You can start the game by running the **main** executable file in a terminal after a successful compilation.


## License
The project created under the **MIT license**. Use at your own risk.
