#include "container.hpp"
#include "graphics.hpp"
#include "libpesta.hpp"
#include "widgets.hpp"

using namespace genv;


/*--------  protected  --------*/

/*--------  public  --------*/

Container::Container(int w, int h) : _width(w), _height(h) {}

void Container::addWidget(Widget* w)
{
	w->parent = this;
	widgets.push_back(w);
}
