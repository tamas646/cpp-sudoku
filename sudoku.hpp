#ifndef SUDOKU_HPP_INCLUDED
#define SUDOKU_HPP_INCLUDED

#include "libpesta.hpp"
#include <vector>
#include <string>

struct Field
{
	int value = 0;
	bool is_controversial = false;
	bool is_fixed = true;
};

class Sudoku
{
protected:
	std::vector<Field> numbers = std::vector<Field>(81, Field());
	std::vector<int> controversial_numbers;
	int misstep_count = 0;

	bool checkNumber(int i);
	bool compareNumbers(int i, int j);

	bool loadBoardFromFile(std::string filename);

	void generateBoard(int n);
	bool fillBoard(int i, int j);
	bool isUnequivocal();

	void emptyBoard();
	void clearControversialList();

public:
	Sudoku();

	void newGame(std::string filename);
	void newGame(int difficulty);
	void guessNumber(int i, int n);
	void guessNumber(int i, int j, int n) { guessNumber(i*9+j, n); };
	std::vector<Field> getNumbers() const { return numbers; }
	Field getNumber(int i) const { return numbers[i]; }
	Field getNumber(int i, int j) const { return getNumber(i*9+j); }
	std::vector<int> controversialNumbers() const { return controversial_numbers; };
	int getMisstepCount() const { return misstep_count; };
	bool isFinished();
};

#endif // SUDOKU_HPP_INCLUDED
