all: main

# List widgets here (1)
main: main.cpp libsudoku.a libcontainer.a libwindow.a libwidgets.a libpesta.a libgraphics.a libgraphics.a widgets/liblabel.a widgets/libbutton.a widgets/libdrawingbox.a widgets/libdigitselector.a widgets/libdropdown.a
# List widgets here (2)
	g++ main.cpp -o main -Wall -L . -lsudoku -lcontainer -lwindow -lwidgets -lpesta -lgraphics -lSDL2 -lSDL2_ttf -L widgets \
		-llabel -lbutton -ldrawingbox -ldigitselector -ldropdown

libsudoku.a: sudoku.cpp
	g++ sudoku.cpp -c -o libsudoku.a -Wall

libcontainer.a: container.cpp
	g++ container.cpp -c -o libcontainer.a -Wall

libwindow.a: window.cpp
	g++ window.cpp -c -o libwindow.a -Wall

libwidgets.a: widgets.cpp
	g++ widgets.cpp -c -o libwidgets.a -Wall

libpesta.a: libpesta.cpp
	g++ libpesta.cpp -c -o libpesta.a -Wall

# Create widgets here (3)
widgets/liblabel.a: widgets/Label.cpp
	g++ widgets/Label.cpp -c -o widgets/liblabel.a -Wall

widgets/libbutton.a: widgets/Button.cpp
	g++ widgets/Button.cpp -c -o widgets/libbutton.a -Wall

widgets/libdrawingbox.a: widgets/DrawingBox.cpp
	g++ widgets/DrawingBox.cpp -c -o widgets/libdrawingbox.a -Wall

widgets/libdigitselector.a: widgets/DigitSelector.cpp
	g++ widgets/DigitSelector.cpp -c -o widgets/libdigitselector.a -Wall

widgets/libdropdown.a: widgets/DropDown.cpp
	g++ widgets/DropDown.cpp -c -o widgets/libdropdown.a -Wall


## DEBUG build

# List widgets here (4)
debug: main.cpp libsudokud.a libcontainerd.a libwindowd.a libwidgetsd.a libpestad.a libgraphics.a widgets/liblabeld.a widgets/libbuttond.a widgets/libdrawingboxd.a widgets/libdigitselectord.a widgets/libdropdownd.a
# List widgets here (5)
	g++ main.cpp -o main -g -Wall -L . -lsudoku -lcontainerd -lwindowd -lwidgetsd -lpestad -lgraphics -lSDL2 -lSDL2_ttf -L widgets \
		-llabeld -lbuttond -ldrawingboxd -ldigitselectord -ldropdownd

libsudokud.a: sudoku.cpp
	g++ sudoku.cpp -c -o libsudokud.a -g -Wall

libcontainerd.a: container.cpp
	g++ container.cpp -c -o libcontainerd.a -g -Wall

libwindowd.a: window.cpp
	g++ window.cpp -c -o libwindowd.a -g -Wall

libwidgetsd.a: widgets.cpp
	g++ widgets.cpp -c -o libwidgetsd.a -g -Wall

libpestad.a: libpesta.cpp
	g++ libpesta.cpp -c -o libpestad.a -g -Wall

# Create widgets here (6)
widgets/liblabeld.a: widgets/Label.cpp
	g++ widgets/Label.cpp -c -o widgets/liblabeld.a -g -Wall

widgets/libbuttond.a: widgets/Button.cpp
	g++ widgets/Button.cpp -c -o widgets/libbuttond.a -g -Wall

widgets/libdrawingboxd.a: widgets/DrawingBox.cpp
	g++ widgets/DrawingBox.cpp -c -o widgets/libdrawingboxd.a -g -Wall

widgets/libdigitselectord.a: widgets/DigitSelector.cpp
	g++ widgets/DigitSelector.cpp -c -o widgets/libdigitselectord.a -g -Wall

widgets/libdropdownd.a: widgets/DropDown.cpp
	g++ widgets/DropDown.cpp -c -o widgets/libdropdownd.a -g -Wall


clean:
	rm -f main libsudoku.a libsudokud.a libcontainer.a libcontainerd.a libwidgets.a libwidgetsd.a libwindow.a libwindowd.a libpesta.a libpestad.a libpestad.a widgets/lib*.a
