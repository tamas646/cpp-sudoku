#ifndef WIDGETS_HPP_INCLUDED
#define WIDGETS_HPP_INCLUDED

#include "libpesta.hpp"
#include "container.hpp"
#include "window.hpp"

/*--------  Class DisplayItem  --------*/

class DisplayItem
{
protected:
	bool _is_hovered = false, _is_focused = false;

	virtual void onFocus() {}
	virtual void onBlur() {}
	virtual void onMouseover(genv::event ev) {}
	virtual void onMouseout(genv::event ev) {}
	virtual void onMousemove(genv::event ev) {}

public:
	virtual void draw(genv::canvas &out) = 0;
	virtual bool isAbove(int x, int y) const = 0;
	virtual inline bool isFocusable() const = 0;
	virtual DisplayItem* getFocusItem(int x, int y) { return this; }

	virtual void handleButtonEvent(genv::event) = 0;
	virtual void handleKeyEvent(genv::event) = 0;

	void focus();
	void blur();
	void mouseover(genv::event ev);
	void mouseout(genv::event ev);
	void mousemove(genv::event ev);
	bool isHovered() const { return _is_hovered; }
	bool isFocused() const { return _is_focused; }
};

/*--------  Class Widget  --------*/

class Widget : public DisplayItem
{
protected:
	Container* parent = 0;
	friend class Popup;

	int _posx = 0, _posy = 0;

	Window* getWindow() const;
	virtual void onButtonEvent(genv::event ev) {}
	virtual void onKeyEvent(genv::event ev) {}

	int getRealPosX() const;
	int getRealPosY() const;

public:
	Widget();
	Widget(Container* c);

	void handleButtonEvent(genv::event ev);
	void handleKeyEvent(genv::event ev);

	void addToContainer(Container* c);
	friend void Container::addWidget(Widget* w);

	void setPos(int x, int y);
	virtual bool canBePlacedHere(int x, int y) const = 0;

	int getPosX() const { return _posx; }
	int getPosY() const { return _posy; }
};

/*--------  Class Popup  --------*/

class Popup : public DisplayItem
{
protected:
	Widget* widget;

	virtual void onButtonEvent(genv::event ev) {}
	virtual void onKeyEvent(genv::event ev) {}
	virtual void onShow() {};
	virtual void onClose() {};
	friend void Window::showPopup(Popup *p);
	friend void Window::closePopup();

public:
	Popup(Widget* w);

	inline bool isFocusable() const { return false; };

	void handleButtonEvent(genv::event ev);
	void handleKeyEvent(genv::event ev);

	void show();
	void close();

	Widget* parent() const { return widget; }
	bool isOpen() const;
};

/*--------  Class Group  --------*/

class Group : public Widget, public Container
{
protected:
	Widget* hover_widget = 0;
	mycanvas group_canvas;

	void onMouseover(genv::event ev);
	void onMouseout(genv::event ev);
	void onMousemove(genv::event ev);
	virtual void onMouseoverGroup(genv::event ev) {};
	virtual void onMouseoutGroup(genv::event ev) {};
	virtual void onMousemoveGroup(genv::event ev) {};

	Widget* getWidgetAt(int x, int y) const;

public:
	Group(int width, int height);
	Group(Container* c, int width, int height);

	void draw(genv::canvas &out);
	bool isAbove(int x, int y) const;
	inline bool isFocusable() const { return false; }
	bool canBePlacedHere(int x, int y) const;

	DisplayItem* getFocusItem(int x, int y);
};

#endif // WIDGETS_HPP_INCLUDED
