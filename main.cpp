#include "graphics.hpp"
#include "libpesta.hpp"
#include "window.hpp"
#include "widgets.hpp"
#include "sudoku.hpp"
#include "widgets/Button.hpp"
#include "widgets/DrawingBox.hpp"
#include "widgets/Label.hpp"
#include "widgets/DigitSelector.hpp"
#include "widgets/DropDown.hpp"
#include <iostream>
#include <string>

using namespace std;
using namespace genv;

class SudokuWindow;

class SudokuGamingArea : public Group
{
	friend class SudokuWindow;

	vector<DigitSelector*> num_inputs;
	DrawingBox* frame;

	const int border_size = 5;
	const int grid3_size = 3;
	const int grid9_size = 1;
	const color frame_color = COLOR_WHITE;
	const int cell_padding_size = 3;
	const float button_size;

protected:

public:
	SudokuGamingArea(Container* c, int w) : Group(c, w, w), button_size((w - 2*border_size - 2*grid3_size - 6*grid9_size - 18*cell_padding_size) / 9.0)
	{
		frame = new DrawingBox(this, _width, _height, [&](mycanvas &out)
		{
			out << frame_color;

			// border
			if(border_size > 0)
			{
				out << move_to(0, 0) << box(_width, border_size);
				out << move_to(0, 0) << box(border_size, _height);
				out << move_to(_width - border_size, 0) << box(border_size, _height);
				out << move_to(0, _height - border_size) << box(_width, border_size);
			}

			// grid3
			if(grid3_size > 0)
			{
				out << move_to(border_size + 3*(button_size + 2*cell_padding_size) + 2*grid9_size, 0) << box(grid3_size, _height);
				out << move_to(border_size + 6*(button_size + 2*cell_padding_size) + 4*grid9_size + grid3_size, 0) << box(grid3_size, _height);
				out << move_to(0, border_size + 3*(button_size + 2*cell_padding_size) + 2*grid9_size) << box(_width, grid3_size);
				out << move_to(0, border_size + 6*(button_size + 2*cell_padding_size) + 4*grid9_size + grid3_size) << box(_width, grid3_size);
			}

			// grid9
			if(grid9_size > 0)
			{
				out << move_to(border_size + 1*(button_size + 2*cell_padding_size) + 0*grid9_size + 0*grid3_size, 0) << box(grid9_size, _height);
				out << move_to(border_size + 2*(button_size + 2*cell_padding_size) + 1*grid9_size + 0*grid3_size, 0) << box(grid9_size, _height);
				out << move_to(border_size + 4*(button_size + 2*cell_padding_size) + 2*grid9_size + 1*grid3_size, 0) << box(grid9_size, _height);
				out << move_to(border_size + 5*(button_size + 2*cell_padding_size) + 3*grid9_size + 1*grid3_size, 0) << box(grid9_size, _height);
				out << move_to(border_size + 7*(button_size + 2*cell_padding_size) + 4*grid9_size + 2*grid3_size, 0) << box(grid9_size, _height);
				out << move_to(border_size + 8*(button_size + 2*cell_padding_size) + 5*grid9_size + 2*grid3_size, 0) << box(grid9_size, _height);
				out << move_to(0, border_size + 1*(button_size + 2*cell_padding_size) + 0*grid9_size + 0*grid3_size) << box(_width, grid9_size);
				out << move_to(0, border_size + 2*(button_size + 2*cell_padding_size) + 1*grid9_size + 0*grid3_size) << box(_width, grid9_size);
				out << move_to(0, border_size + 4*(button_size + 2*cell_padding_size) + 2*grid9_size + 1*grid3_size) << box(_width, grid9_size);
				out << move_to(0, border_size + 5*(button_size + 2*cell_padding_size) + 3*grid9_size + 1*grid3_size) << box(_width, grid9_size);
				out << move_to(0, border_size + 7*(button_size + 2*cell_padding_size) + 4*grid9_size + 2*grid3_size) << box(_width, grid9_size);
				out << move_to(0, border_size + 8*(button_size + 2*cell_padding_size) + 5*grid9_size + 2*grid3_size) << box(_width, grid9_size);
			}
		});

		for(int i = 0; i < 81; ++i)
		{
			int button_x_db = i%9;
			int button_y_db = i/9;
			int grid3_x_db = button_x_db/3;
			int grid3_y_db = button_y_db/3;
			int grid9_x_db = (button_x_db - grid3_x_db);
			int grid9_y_db = (button_y_db - grid3_y_db);
			DigitSelector* a = new DigitSelector(this, button_size, button_size, false);
			a->setColor(COLOR_WHITE);
			a->setFontSize(30);
			a->setBorderSize(0);
			a->setPos(border_size + cell_padding_size + button_x_db*(button_size + 2*cell_padding_size) + grid3_x_db*grid3_size + grid9_x_db*grid9_size, border_size + cell_padding_size + button_y_db*(button_size + 2*cell_padding_size) + grid3_y_db*grid3_size + grid9_y_db*grid9_size);
			num_inputs.push_back(a);
		}
	}
};

class SudokuWindow : public Window
{
	Sudoku* sudoku;
	SudokuGamingArea* gaming_area;
	Button* new_game_button;
	DropDown* game_type_select;
	DropDown* game_difficulty_select;
	Button* exit_button;
	Label* status_label;
	DrawingBox* status_bar_line;

	static const int status_bar_height = 40;
	static const int status_bar_line_size = 3;
	static const int status_bar_buttons_width = 100;
	static const int status_bar_select_width = 200;
	static const int status_bar_select_padding = 10;
	static const int status_bar_label_width = 300;
	static const int status_bar_margin = 10;
	static const int gaming_area_size = 600;
	static const int gaming_area_margin = 30;
	const color status_bar_line_color = COLOR_WHITE;

	vector<string> game_types;
	vector<string> game_difficulties;
	vector<int> game_difficulty_nums;
	vector<string> game_files;

protected:
	void onEvent(event ev, DisplayItem* focus_item)
	{
		// global event handler
	}

	static inline int calcWindowWidth()
	{
		return max(2*gaming_area_margin + gaming_area_size, 2*status_bar_margin + 2*status_bar_buttons_width + 2*status_bar_select_width + 2*status_bar_select_padding + status_bar_label_width);
	}
	static inline int calcWindowHeight()
	{
		return 2*status_bar_margin + status_bar_height + status_bar_line_size + 2*gaming_area_margin + gaming_area_size;
	}

public:
	SudokuWindow() : Window(calcWindowWidth(), calcWindowHeight())
	{
		game_types.push_back("Generált");
		game_types.push_back("Fájlból");

		game_difficulties.push_back("Könnyű - 56");
		game_difficulties.push_back("Közepes - 48");
		game_difficulties.push_back("Nehéz - 39");
		game_difficulty_nums.push_back(56);
		game_difficulty_nums.push_back(48);
		game_difficulty_nums.push_back(39);

		game_files.push_back("easy-1.txt");
		game_files.push_back("easy-2.txt");
		game_files.push_back("easy-3.txt");
		game_files.push_back("easy-4.txt");
		game_files.push_back("easy-5.txt");
		game_files.push_back("medium-1.txt");
		game_files.push_back("medium-2.txt");
		game_files.push_back("medium-3.txt");
		game_files.push_back("medium-4.txt");
		game_files.push_back("medium-5.txt");
		game_files.push_back("hard-1.txt");
		game_files.push_back("hard-2.txt");
		game_files.push_back("hard-3.txt");
		game_files.push_back("hard-4.txt");
		game_files.push_back("hard-5.txt");
		game_files.push_back("very-hard-1.txt");
		game_files.push_back("very-hard-2.txt");
		game_files.push_back("very-hard-3.txt");
		game_files.push_back("very-hard-4.txt");
		game_files.push_back("very-hard-5.txt");
		game_files.push_back("insane-1.txt");
		game_files.push_back("insane-2.txt");
		game_files.push_back("insane-3.txt");
		game_files.push_back("insane-4.txt");
		game_files.push_back("insane-5.txt");
		game_files.push_back("inhuman-1.txt");
		game_files.push_back("inhuman-2.txt");
		game_files.push_back("inhuman-3.txt");
		game_files.push_back("inhuman-4.txt");
		game_files.push_back("inhuman-5.txt");

		sudoku = new Sudoku();
		gaming_area = new SudokuGamingArea(this, gaming_area_size);
		gaming_area->setPos((_width - gaming_area_size) / 2, 2*status_bar_margin + status_bar_height + status_bar_line_size + gaming_area_margin);
		new_game_button = new Button(this, "Új játék", status_bar_buttons_width, status_bar_height);
		new_game_button->setPos(status_bar_margin, status_bar_margin);
		game_type_select = new DropDown(this, game_types);
		game_type_select->setBoxWidth(status_bar_select_width);
		game_type_select->setPos(status_bar_margin + status_bar_buttons_width + status_bar_select_padding, status_bar_margin);
		game_difficulty_select = new DropDown(this, game_difficulties);
		game_difficulty_select->setBoxWidth(status_bar_select_width);
		game_difficulty_select->setPos(status_bar_margin + status_bar_buttons_width + 2*status_bar_select_padding + status_bar_select_width, status_bar_margin);
		exit_button = new Button(this, "Kilépés", status_bar_buttons_width, status_bar_height);
		exit_button->setPos(_width - status_bar_margin - status_bar_buttons_width, status_bar_margin);
		status_label = new Label(this, "", Label::center, status_bar_label_width, status_bar_height);
		status_label->setPos(_width - status_bar_label_width - status_bar_margin - status_bar_buttons_width, status_bar_margin);
		status_bar_line = new DrawingBox(this, _width, status_bar_line_size, [&](canvas &out)
		{
			out << move_to(0, 0) << status_bar_line_color << box(_width, status_bar_line_size);
		});
		status_bar_line->setPos(0, 2*status_bar_margin + status_bar_height);

		// Game type select event
		game_type_select->onChange([&](int index, string value)
		{
			if(index == 0)
			{
				game_difficulty_select->setList(game_difficulties);
			}
			else
			{
				game_difficulty_select->setList(game_files);
			}
		});

		// Button events
		new_game_button->onPress([this]()
		{
			if(game_type_select->selectedIndex() == 0)
			{
				cout << "A feladvány generálása nincs teljesen befejezve, így nem használható" << endl; return;
				sudoku->newGame(game_difficulty_nums[game_difficulty_select->selectedIndex()]);
			}
			else
			{
				sudoku->newGame("boards/" + game_difficulty_select->value());
			}
			refresh_widgets();
		});
		exit_button->onPress([this]()
		{
			exit(0);
		});
		for(int i = 0; i < int(gaming_area->num_inputs.size()); ++i)
		{
			gaming_area->num_inputs[i]->onChange([this, i](int n)
			{
				sudoku->guessNumber(i, n);
				refresh_widgets();
			});
		}

		refresh_widgets();
	}

	void refresh_widgets()
	{
		if(sudoku->isFinished())
		{
			status_label->setColor(COLOR_GREEN);
			status_label->setValue("Gratulálok! (" + to_string(sudoku->getMisstepCount()) + " hiba)");
			for(int i = 0; i < int(gaming_area->num_inputs.size()); ++i)
			{
				Field a = sudoku->getNumber(i);
				gaming_area->num_inputs[i]->setValue(a.value);
				gaming_area->num_inputs[i]->setColor(a.is_fixed ? COLOR_WHITE : color(4, 142, 234));
				gaming_area->num_inputs[i]->disable();
			}
		}
		else
		{
			status_label->setColor(COLOR_WHITE);
			status_label->setValue("Hibázások száma: " + to_string(sudoku->getMisstepCount()));
			for(int i = 0; i < int(gaming_area->num_inputs.size()); ++i)
			{
				Field a = sudoku->getNumber(i);
				gaming_area->num_inputs[i]->setValue(a.value);
				gaming_area->num_inputs[i]->setColor(a.is_controversial ? COLOR_RED : (a.is_fixed ? COLOR_WHITE : color(4, 142, 234)));
				gaming_area->num_inputs[i]->setStatus(!a.is_fixed);
			}
		}
	}
};

int main()
{
	SudokuWindow window;

	window.refresh();

	return window.run();
}
