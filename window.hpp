#ifndef WINDOW_HPP_INCLUDED
#define WINDOW_HPP_INCLUDED

#include "graphics.hpp"
#include "container.hpp"
#include <vector>

class DisplayItem;
class Popup;

class Window : public Container
{
	bool _running = true;
	int _exitcode = 0;
	Popup* _popup = 0;

protected:
	virtual void onEvent(genv::event ev, DisplayItem* focus_item) {};

public:
	Window(int w, int h);
	void showPopup(Popup* p);
	void closePopup();
	bool isOpen(const Popup* p) const;
	int run();
	void refresh() const;
	void exit(int code);
};

#endif // WINDOW_HPP_INCLUDED
