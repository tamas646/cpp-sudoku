#ifndef CONTAINER_HPP_INCLUDED
#define CONTAINER_HPP_INCLUDED

#include <vector>

class Widget;

class Container
{
protected:
	std::vector<Widget*> widgets;
	int _width, _height;

public:
	Container(int w, int h);
	virtual ~Container() = default; // required to be polymorphic

	void addWidget(Widget* w);

	int getWidth() const { return _width; }
	int getHeight() const { return _height; }
};

#endif // CONTAINER_HPP_INCLUDED
