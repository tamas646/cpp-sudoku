#include "libpesta.hpp"

using namespace genv;
using namespace std;

URandom::URandom(int n) : _n(n) {}
int URandom::next()
{
	if(_n <= int(_p.size())) return -1;

	int r = rand() % (_n - _p.size());
	do r = (r+1)%_n;
	while(_p.find(r) != _p.end());
	_p.insert(r);
	return r;
}
void URandom::reset()
{
	_p.clear();
}
void URandom::skip(int r)
{
	if(_p.find(r) == _p.end()) _p.insert(r);
}

int Font::charHeight() const
{
	gout.load_font(name, size);
	return gout.cdescent() + gout.cascent();
}
int Font::stringWidth(std::string s) const
{
	gout.load_font(name, size);
	return gout.twidth(s);
}
string Font::shortenText(string t, int max_width) const
{
	string text = t;
	int sw = stringWidth(text);
	for(int i = 0; i < int(t.size()) && sw > max_width; ++i)
	{
		text = text.substr(0, text.size()-1);
		sw = stringWidth(text + "...");
	}
	if(text != t) text += "...";
	return text;
}

void mygroutput::open(int XX, int YY, color background)
{
	_XX = XX;
	_YY = YY;
	bg = background;
	gout.open(_XX, _YY);
}
void mygroutput::clear()
{
	gout << move_to(0, 0) << bg << box(_XX, _YY);
}

mygroutput mygout;

double vect::distance(const vect& a)
{
	return sqrt(pow(this->x - a.x, 2) + pow(this->y - a.y, 2));
}
vect vect::operator+ (const vect& a)
{
	return vect(this->x + a.x, this->y + a.y);
}
void vect::operator+= (const vect& a)
{
	x += a.x;
	y += a.y;
}
vect vect::operator* (double a)
{
	return vect(this->x * a, this->y * a);
}

vect operator* (const vect& v, const vect4& m)
{
	return vect(v.x * m.a + v.y * m.b, v.x * m.c + v.y * m.d);
}

void polygon::operator () (canvas& out)
{
	if(fill && r <= 0)
	{
		out << dot;
		return;
	}
	float alpha = 2 * M_PI / n;
	float cos_alpha = cos(alpha);
	float sin_alpha = sin(alpha);
	vect4 rotate(cos_alpha, -sin_alpha, sin_alpha, cos_alpha);
	// float x = r * sqrt(2 * (1 - cos_alpha));
	float x = 2 * r * tan(alpha / 2);
	vect center(gout.x(), gout.y());
	out << genv::move(r - 1, - x / 2);
	vect lv(0, x);
	vect pos(gout.x(), gout.y());
	out << line(lv.x, lv.y);
	for(int i = 1; i < n; ++i)
	{
		pos.x += lv.x;
		pos.y += lv.y;
		lv = lv * rotate;
		out << move_to(round(pos.x), round(pos.y)) << dot << line(round(lv.x), round(lv.y)) << dot;
		// out << line(lv.x, lv.y);
	}
	if(s > 0)
	{
		out << move_to(center.x, center.y) << polygon(r - precision, n, s - 1, fill, fill_color, precision);
	}
	else if(fill)
	{
		out << fill_color << move_to(center.x, center.y) << polygon(r - precision, n, 0, true, fill_color, precision);
	}
}

void rectangle::operator () (canvas& out)
{
	// out << line(w, 0) << line(0, h) << line(-w, 0) << line(0, -h); // @nadak sad it's slow :)
	// color c = fill ? fill_color : mygout.getBgColor();
	// out << box(w, h) << move(-w+s+1, -h+s+1) << c << box(w-2*s, h-2*s);
	out << box(w, s) << genv::move(-s+1, -s+1) << box(s, h) << genv::move(-w+1, -h+1) << box(s, h) << genv::move(-s+1, -s+1) << box(w, s);
	if(fill) out << genv::move(-w+s, -h+s) << fill_color << box(w-s, h-s);
}

void circle::operator () (canvas& out)
{
	out << genv::move(-r, -r);
	for(int i = -r; i <= r; ++i)
	{
		for(int j = -r; j <= r; ++j)
		{
			int d = pow(i, 2) + pow(j, 2);
			if(d <= pow(r, 2))
			{
				if(d >= pow(r - s, 2))
				{
					out << line_color << dot;
				}
				else if(fill)
				{
					out << fill_color << dot;
				}
			}
			out << genv::move(0, 1);
		}
		out << genv::move(1, - 2 * r - 1);
	}
}

void circle2::operator () (canvas& out)
{
	if(fill)
	{
		out << polygon(r, 12, s, true, fill_color, 0.25);
	}
	else
	{
		out << polygon(r, 12, s, false, fill_color, 0.25);
	}
}

void triangle::operator () (canvas& out)
{
	for(int i = 0; i < w; ++i)
	{
		for(int j = 0; j < h; ++j)
		{
			int left_line = (h-j)*w / (2*h);
			int right_line = (h+j)*w / (2*h);
			int dx2 = pow(s, 2) * (pow(h, 2) + pow(w, 2)/4) / pow(h, 2);
			if(left_line <= i && i <= right_line)
			{
				if(j <= s || j >= h - s || pow(i - left_line, 2) <= dx2 || pow(right_line - i, 2) <= dx2)
				{
					out << line_color << dot;
				}
				else if(fill)
				{
					out << fill_color << dot;
				}
			}
			out << genv::move(0, 1);
		}
		out << genv::move(1, -h);
	}
}
