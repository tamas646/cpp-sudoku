#include "widgets.hpp"
#include "container.hpp"
#include "graphics.hpp"
#include "libpesta.hpp"
#include "window.hpp"
#include <algorithm>
#include <string>

using namespace genv;
using namespace std;

/*--------  DisplayItem: public  --------*/

void DisplayItem::focus()
{
	_is_focused = true;
	onFocus();
}
void DisplayItem::blur()
{
	_is_focused = false;
	onBlur();
}

void DisplayItem::mouseover(event ev)
{
	_is_hovered = true;
	onMouseover(ev);
}
void DisplayItem::mouseout(event ev)
{
	_is_hovered = false;
	onMouseout(ev);
}
void DisplayItem::mousemove(event ev)
{
	onMousemove(ev);
}

/*--------  Widget: protected  --------*/

Window* Widget::getWindow() const
{
	if(parent)
	{
		Window* window = dynamic_cast<Window*>(parent);
		if(window) return window;
		Widget* widget = dynamic_cast<Widget*>(parent);
		if(widget) return widget->getWindow();
	}
	return 0;
}

int Widget::getRealPosX() const
{
	if(parent)
	{
		Window* window = dynamic_cast<Window*>(parent);
		if(window) return _posx;
		Widget* widget = dynamic_cast<Widget*>(parent);
		if(widget) return _posx + widget->getRealPosX();
	}
	return -1;
}
int Widget::getRealPosY() const
{
	if(parent)
	{
		Window* window = dynamic_cast<Window*>(parent);
		if(window) return _posy;
		Widget* widget = dynamic_cast<Widget*>(parent);
		if(widget) return _posy + widget->getRealPosY();
	}
	return -1;
}

/*--------  Widget: public  --------*/

Widget::Widget() {}
Widget::Widget(Container* c)
{
	addToContainer(c);
}

void Widget::handleButtonEvent(event ev)
{
	onButtonEvent(ev);
	Widget* widget = dynamic_cast<Widget*>(parent);
	if(widget) widget->handleButtonEvent(ev);
}
void Widget::handleKeyEvent(event ev)
{
	onKeyEvent(ev);
	Widget* widget = dynamic_cast<Widget*>(parent);
	if(widget) widget->handleKeyEvent(ev);
}

void Widget::addToContainer(Container* c)
{
	c->addWidget(this);
}

void Widget::setPos(int x, int y)
{
	if(!parent) return;

	if(_posx < 0) _posx = 0;
	if(_posx >= parent->getWidth()) _posx = parent->getWidth() - 1;
	if(_posy < 0) _posy = 0;
	if(_posy >= parent->getHeight()) _posy = parent->getHeight() - 1;

	if(canBePlacedHere(x, y))
	{
		_posx = x;
		_posy = y;
	}
}

/*--------  Popup: protected  --------*/

/*--------  Popup: public  --------*/

Popup::Popup(Widget* w)
{
	widget = w;
}

void Popup::handleButtonEvent(event ev) { onButtonEvent(ev); }
void Popup::handleKeyEvent(event ev) { onKeyEvent(ev); }

void Popup::show()
{
	Window* w = widget->getWindow();
	if(w) w->showPopup(this);
}
void Popup::close()
{
	Window* w = widget->getWindow();
	if(w && w->isOpen(this)) w->closePopup();
}

bool Popup::isOpen() const
{
	Window* w = widget->getWindow();
	return w && w->isOpen(this);
}

/*--------  Group: protected  --------*/

void Group::onMouseover(event ev) { onMouseoverGroup(ev); }
void Group::onMouseout(event ev)
{
	if(hover_widget) hover_widget->mouseout(ev);
	hover_widget = 0;
	onMouseoutGroup(ev);
}
void Group::onMousemove(event ev)
{
	ev.pos_x -= _posx;
	ev.pos_y -= _posy;
	Widget* hovered = getWidgetAt(ev.pos_x, ev.pos_y);
	if(hovered)
	{
		if(hovered != hover_widget)
		{
			if(hover_widget) hover_widget->mouseout(ev);
			hover_widget = hovered;
			hover_widget->mouseover(ev);
		}
		hover_widget->mousemove(ev);
	}
	else if(hover_widget)
	{
		hover_widget->mouseout(ev);
		hover_widget = 0;
	}
	ev.pos_x += _posx;
	ev.pos_y += _posy;
	onMousemoveGroup(ev);
}

Widget* Group::getWidgetAt(int x, int y) const
{
	for(int i = widgets.size() - 1; i >= 0; --i)
	{
		if(widgets[i]->isAbove(x, y))
		{
			return widgets[i];
		}
	}
	return 0;
}

/*--------  Group: public  --------*/

Group::Group(int width, int height) : Container(width, height) { group_canvas = mycanvas(_width, _height); }
Group::Group(Container* c, int width, int height) : Widget(c), Container(width, height) { group_canvas = mycanvas(_width, _height); }

void Group::draw(canvas &out)
{
	group_canvas.clear();
	for(Widget* a : widgets)
	{
		a->draw(group_canvas);
	}
	out << stamp(group_canvas, _posx, _posy);
}

bool Group::isAbove(int x, int y) const
{
	return x >= _posx && y >= _posy && x < _posx + _width && y < _posy + _height;
}

bool Group::canBePlacedHere(int x, int y) const
{
	return parent->getWidth() - x > _width && parent->getHeight() - y > _height;
}

DisplayItem* Group::getFocusItem(int x, int y)
{
	Widget* widget = getWidgetAt(x - _posx, y - _posy);
	if(widget) return widget->getFocusItem(x, y);
	return widget;
}
