#include "DigitSelector.hpp"
#include <string>

using namespace std;
using namespace genv;

/*--------  DigitSelector: protected  --------*/

void DigitSelector::render()
{
	_rendered_digitselector.clear();
	_rendered_digitselector_opened.clear();

	// background
	_rendered_digitselector << move_to(0, 0) << background_color << box(_width, _height);
	_rendered_digitselector_opened << move_to(0, 0) << _textcolor << box(_width, _height);

	// border
	if(_border_size > 0)
	{
		_rendered_digitselector << move_to(0, 0) << _border_color << rectangle(_width, _height, _border_size);
		_rendered_digitselector_opened << move_to(0, 0) << _border_color << rectangle(_width, _height, _border_size);
	}

	// text
	if(_display_zero || _value != 0)
	{
		string text = to_string(_value);
		int sw = _font.stringWidth(text);
		int ch = _font.charHeight();
		_rendered_digitselector << move_to(_width/2 - sw/2, (_height - ch)/2) << _textcolor << genv::text(text);
		_rendered_digitselector_opened << move_to(_width/2 - sw/2, (_height - ch)/2) << background_color << genv::text(text);
	}
}

void DigitSelector::showPopup()
{
	Window* window = getWindow();
	if(!window) return;

	int real_posy = getRealPosY();
	int available_height_below = window->getHeight() - real_posy - _height;
	int available_height_above = real_posy;
	if(available_height_below >= popup->height)
	{
		popup->_show_below = true;
		popup->show();
	}
	else if(available_height_above >= popup->height)
	{
		popup->_show_below = false;
		popup->show();
	}
}
void DigitSelector::closePopup()
{
	popup->close();
}

void DigitSelector::onButtonEvent(event ev)
{
	if(ev.button == btn_left)
	{
		if(_enabled)
		{
			if(!popup->isOpen()) showPopup();
			else _close_popup_on_release = true;
		}
	}
	else if(ev.button == -btn_left)
	{
		if(_enabled)
		{
			if(_close_popup_on_release)
			{
				if(popup->isOpen()) closePopup();
				_close_popup_on_release = false;
			}
		}
	}
}

void DigitSelector::onKeyEvent(event ev)
{
	if(ev.keycode == key_space)
	{
		if(_enabled)
		{
			if(!popup->isOpen()) showPopup();
		}
	}
	else if(ev.keycode == key_enter)
	{
		if(_enabled)
		{
			if(!popup->isOpen()) showPopup();
		}
	}
}

/*--------  DigitSelector: public  --------*/

DigitSelector::DigitSelector() { construct(default_width, default_height); }
DigitSelector::DigitSelector(Container* c) : Widget(c) { construct(default_width, default_height); }
DigitSelector::DigitSelector(bool display_zero) : _display_zero(display_zero) { construct(default_width, default_height); }
DigitSelector::DigitSelector(Container* c, bool display_zero) : Widget(c), _display_zero(display_zero) { construct(default_width, default_height); }
DigitSelector::DigitSelector(int width, int height) { construct(width, height); }
DigitSelector::DigitSelector(Container* c, int width, int height) : Widget(c) { construct(width, height); }
DigitSelector::DigitSelector(int width, int height, bool display_zero) : _display_zero(display_zero) { construct(width, height); }
DigitSelector::DigitSelector(Container* c, int width, int height, bool display_zero) : Widget(c), _display_zero(display_zero) { construct(width, height); }
// construct - protected
void DigitSelector::construct(int width, int height)
{
	_width = width;
	_height = height;
	popup = new DigitSelectorPopup(this);
	_rendered_digitselector.open_with_font(_width, _height, _font);
	_rendered_digitselector_opened.open_with_font(_width, _height, _font);
	render();
}

void DigitSelector::draw(canvas &out)
{
	if(popup->isOpen())
	{
		out << stamp(_rendered_digitselector_opened, _posx, _posy);
	}
	else
	{
		out << stamp(_rendered_digitselector, _posx, _posy);
	}
}

bool DigitSelector::isAbove(int x, int y) const
{
	return x >= _posx && y >= _posy && x < _posx + _width && y < _posy + _height;
}

bool DigitSelector::canBePlacedHere(int x, int y) const
{
	return parent->getWidth() - x >= _width && parent->getHeight() - y >= _height;
}

void DigitSelector::setFont(Font font)
{
	_font = font;
	_rendered_digitselector.loadfont(_font);
	_rendered_digitselector_opened.loadfont(_font);
	render();
}
void DigitSelector::setFontName(string name)
{
	_font.name = name;
	_rendered_digitselector.loadfont(_font);
	_rendered_digitselector_opened.loadfont(_font);
	render();
}
void DigitSelector::setFontSize(int size)
{
	_font.size = size;
	_rendered_digitselector.loadfont(_font);
	_rendered_digitselector_opened.loadfont(_font);
	render();
}

void DigitSelector::setColor(color c)
{
	_textcolor = c;
	render();
}

void DigitSelector::setBorderSize(int size)
{
	_border_size = size;
	render();
}
void DigitSelector::setBorderColor(color c)
{
	_border_color = c;
	render();
}

void DigitSelector::setValue(int value, bool trigger_event)
{
	if(value >= 0 && value <= 9 && value != _value)
	{
		_value = value;
		if(trigger_event && onchange_fun) onchange_fun(_value);
		render();
	}
}

void DigitSelector::onChange(function<void(int)> fun)
{
	onchange_fun = fun;
}

void DigitSelector::setStatus(bool enabled)
{
	if(popup->isOpen()) popup->close();
	_enabled = enabled;
	render();
}

/*--------  DigitSelectorPopup: protected  --------*/

bool DigitSelectorPopup::is_initialized = false;
color DigitSelectorPopup::border_color = color(100, 100, 100);
Font DigitSelectorPopup::font = Font("LiberationMono-Regular.ttf", 18);
color DigitSelectorPopup::background_color = COLOR_BLACK;

mycanvas DigitSelectorPopup::rendered_popup;
Group* DigitSelectorPopup::num_button_container;
vector<Button*> DigitSelectorPopup::num_buttons;

void DigitSelectorPopup::initialize()
{
	num_button_container = new Group(3*button_size + 2*button_padding, 4*button_size + 3*button_padding);
	num_buttons.push_back(new Button(num_button_container, 3*button_size + 2*button_padding, button_size));
	num_buttons[0]->setFont(font);
	for(int i = 1; i < 10; ++i)
	{
		Button* a = new Button(num_button_container, to_string(i), button_size, button_size);
		a->setFont(font);
		num_buttons.push_back(a);
	}
	num_buttons[0]->setPos(0, 3*(button_size + button_padding));
	for(int i = 0; i < 9; ++i)
	{
		num_buttons[i+1]->setPos(i%3*(button_size + button_padding), i/3*(button_size + button_padding));
	}

	is_initialized = true;
}

void DigitSelectorPopup::render()
{
	rendered_popup.clear();

	// background
	rendered_popup << move_to(0, 0) << background_color << box(width, height);

	// border
	rendered_popup << border_color;
	rendered_popup << move_to(0, 0) << box(width, border_size);
	rendered_popup << move_to(0, 0) << box(border_size, height);
	rendered_popup << move_to(0, height - border_size) << box(width, border_size);
	rendered_popup << move_to(width - border_size, 0) << box(border_size, height);
}

void DigitSelectorPopup::onButtonEvent(event ev)
{
	int cx = _posx + border_size + button_padding;
	int cy = _posy + border_size + button_padding;
	if(ev.pos_x >= cx && ev.pos_x < cx + num_button_container->getWidth() && ev.pos_y >= cy && ev.pos_y < cy + num_button_container->getHeight())
	{
		ev.pos_x -= cx;
		ev.pos_y -= cy;
		for(int i = 0; i < int(num_buttons.size()); ++i)
		{
			if(num_buttons[i]->isAbove(ev.pos_x, ev.pos_y))
			{
				num_buttons[i]->handleButtonEvent(ev);
			}
		}
	}
}

void DigitSelectorPopup::onKeyEvent(event ev)
{
	// extra feature
}

void DigitSelectorPopup::onShow()
{
	int real_posy = digitselector->getRealPosY();
	int window_width = digitselector->getWindow()->getWidth();
	_posx = digitselector->getRealPosX() + digitselector->getWidth()/2 - width/2;
	if(_posx < 0) _posx = 0;
	if(_posx + width > window_width) _posx = window_width - width;
	if(_show_below) _posy = real_posy + digitselector->getHeight();
	else _posy = real_posy - height;

	if(digitselector->_display_zero)
	{
		num_buttons[0]->setLabel("0");
		num_buttons[0]->setFontSize(font.size);
	}
	else
	{
		num_buttons[0]->setLabel("Törlés");
		num_buttons[0]->setFontSize(font.size - 3);
	}

	for(int i = 0; i < 10; ++i)
	{
		num_buttons[i]->onRelease([this, i]()
		{
			digitselector->setValue(i, true);
			close();
		});
	}

	rendered_popup.open(width, height);
	render();
}

/*--------  DigitSelectorPopup: public  --------*/

DigitSelectorPopup::DigitSelectorPopup(DigitSelector* w) : Popup(w)
{
	if(!is_initialized) initialize();

	digitselector = w;
}

void DigitSelectorPopup::draw(canvas &out)
{
	canvas c(num_button_container->getWidth(), num_button_container->getHeight());

	num_button_container->draw(c);

	out << stamp(rendered_popup, _posx, _posy) << stamp(c, _posx + border_size + button_padding, _posy + border_size + button_padding);
}

bool DigitSelectorPopup::isAbove(int x, int y) const
{
	return x >= _posx && y >= _posy && x < _posx + width && y < _posy + height;
}
