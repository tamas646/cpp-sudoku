#include "Button.hpp"
#include "../window.hpp"

using namespace std;
using namespace genv;

/*--------  protected  --------*/

void Button::render()
{
	_rendered_button.clear();
	_rendered_button_pressed.clear();

	// background
	_rendered_button << move_to(0, 0) << background_color << box(_width, _height);
	_rendered_button_pressed << move_to(0, 0) << _textcolor << box(_width, _height);

	// border
	_rendered_button << move_to(0, 0) << border_color << rectangle(_width, _height, border_size);
	_rendered_button_pressed << move_to(0, 0) << border_color << rectangle(_width, _height, border_size);

	// text
	int sw = _font.stringWidth(_text);
	int ch = _font.charHeight();
	_rendered_button << move_to(_width/2 - sw/2, (_height - ch)/2) << _textcolor << text(_text);
	_rendered_button_pressed << move_to(_width/2 - sw/2, (_height - ch)/2) << background_color << text(_text);
}

void Button::autoScale()
{
	if(!_fixed_size)
	{
		_width = _font.stringWidth(_text);
		_height = _font.charHeight();
	}
}

void Button::onButtonEvent(event ev)
{
	if(ev.button == btn_left)
	{
		_button_selecting = true;
		if(!(_key_space_selecting || _key_enter_selecting))
		{
			_onpress_fun();
		}
	}
	else if(ev.button == -btn_left)
	{
		_button_selecting = false;
		if(!(_key_space_selecting || _key_enter_selecting))
		{
			_onrelease_fun();
		}
	}
}
void Button::onKeyEvent(event ev)
{
	if(ev.keycode == key_space)
	{
		if(!_key_space_selecting)
		{
			_key_space_selecting = true;
			if(!(_button_selecting || _key_enter_selecting))
			{
				_onpress_fun();
			}
		}
	}
	else if(ev.keycode == -key_space)
	{
		_key_space_selecting = false;
		if(!(_button_selecting || _key_enter_selecting))
		{
			_onrelease_fun();
		}
	}
	else if(ev.keycode == key_enter)
	{
		if(!_key_enter_selecting)
		{
			_key_enter_selecting = true;
			if(!(_button_selecting || _key_space_selecting))
			{
				_onpress_fun();
			}
		}
	}
	else if(ev.keycode == -key_enter)
	{
		_key_enter_selecting = false;
		if(!(_button_selecting || _key_space_selecting))
		{
			_onrelease_fun();
		}
	}
}

/*--------  public  --------*/

Button::Button() { construct(default_text, default_width, default_height); }
Button::Button(Container* c) : Widget(c) { construct(default_text, default_width, default_height); }
Button::Button(string text) { construct(text); }
Button::Button(Container* c, string text) : Widget(c) { construct(text); }
Button::Button(int width, int height) { _fixed_size = true; construct(default_text, width, height); }
Button::Button(Container* c, int width, int height) : Widget(c) { _fixed_size = true; construct(default_text, width, height); }
Button::Button(string text, int width, int height) { _fixed_size = true; construct(text, width, height); }
Button::Button(Container* c, string text, int width, int height) : Widget(c) { _fixed_size = true; construct(text, width, height); }
// construct - protected
void Button::construct(string text)
{
	int width = _font.stringWidth(text) + 2*(border_size + padding_size);
	int height = _font.charHeight() + 2*(border_size + padding_size);
	construct(text, width, height);
}
void Button::construct(string text, int width, int height)
{
	_width = width;
	_height = height;
	_text = text;
	_rendered_button.open_with_font(_width, _height, _font);
	_rendered_button_pressed.open_with_font(_width, _height, _font);
	render();
}

void Button::draw(canvas &out)
{
	if(_button_selecting || _key_space_selecting || _key_enter_selecting)
	{
		out << stamp(_rendered_button_pressed, _posx, _posy);
	}
	else
	{
		out << stamp(_rendered_button, _posx, _posy);
	}
}

bool Button::isAbove(int x, int y) const
{
	return x >= _posx && y >= _posy && x < _posx + _width && y < _posy + _height;
}

bool Button::canBePlacedHere(int x, int y) const
{
	return parent->getWidth() - x >= _width && parent->getHeight() - y >= _height;
}

void Button::setFont(Font font)
{
	_font = font;
	_rendered_button.loadfont(_font);
	_rendered_button_pressed.loadfont(_font);
	autoScale();
	render();
}
void Button::setFontName(string name)
{
	_font.name = name;
	_rendered_button.loadfont(_font);
	_rendered_button_pressed.loadfont(_font);
	autoScale();
	render();
}
void Button::setFontSize(int size)
{
	_font.size = size;
	_rendered_button.loadfont(_font);
	_rendered_button_pressed.loadfont(_font);
	autoScale();
	render();
}

void Button::setColor(color c)
{
	_textcolor = c;
	render();
}

void Button::setLabel(string text)
{
	_text = text;
	autoScale();
	render();
}

void Button::onPress(std::function<void()> fun)
{
	_onpress_fun = fun;
}
void Button::onRelease(std::function<void()> fun)
{
	_onrelease_fun = fun;
}
