#ifndef WIDGETS_DrOPDOWN_HPP_INCLUDED
#define WIDGETS_DrOPDOWN_HPP_INCLUDED

#include "../widgets.hpp"
#include <vector>
#include <functional>

class DropDownPopup;

class DropDown : public Widget
{
protected:
	const int min_width = 40;
	const int border_size = 1;
	const genv::color border_color = COLOR_WHITE;
	const int padding_size = 5;
	const Font default_font = Font("LiberationSans-Regular.ttf", 20);
	const int arrow_width = 12;
	const int arrow_height = 6;
	const int arrow_padding = 3;
	const genv::color arrow_color = COLOR_RED;
	const genv::color text_color = COLOR_WHITE;
	const std::vector<std::string> default_list;

	mycanvas _rendered_dropdown;
	int _width, _height;
	bool _fixed_width = false;
	Font _font = default_font;
	std::vector<std::string> _list;
	int _index;
	DropDownPopup* popup;
	friend class DropDownPopup;
	std::function<void(int, std::string)> onchange_fun;

	void construct(std::vector<std::string> list);
	void render();
	void calcBoxSize();
	int calcBoxWidth() const;
	void showPopup();
	void closePopup();

	void onButtonEvent(genv::event ev);
	void onKeyEvent(genv::event ev);

public:
	DropDown();
	DropDown(Container* c);
	DropDown(std::vector<std::string> list);
	DropDown(Container* c, std::vector<std::string> list);

	void draw(genv::canvas &out);
	bool isAbove(int x, int y) const;
	inline bool isFocusable() const { return true; };
	bool canBePlacedHere(int x, int y) const;

	void setList(std::vector<std::string> list);
	void setIndex(int index);
	void setBoxWidth(int width);

	int getWidth() const { return _width; };
	int getHeight() const { return _height; };

	std::string value() const;
	int selectedIndex() const;

	void clearItems();
	void addItem(std::string item);
	void removeItem();

	void onChange(std::function<void(int, std::string)> fun);
};

class DropDownPopup : public Popup
{
protected:
	DropDown* dropdown;
	friend void DropDown::showPopup();

	const int min_displayed_items = 3;
	const genv::color background_color = COLOR_BLACK;
	const int border_size = 1;
	const genv::color border_color = COLOR_WHITE;
	const int item_padding_size = 3;
	const int item_border_size = 1;
	const genv::color selected_color = COLOR_RED;
	const genv::color text_color = COLOR_WHITE;

	static mycanvas rendered_popup;

	int _posx, _posy;
	int _width, _height;
	bool _show_below = true;
	int _displayed_items, _start_index;
	int _focus_index, _keyboard_index;
	bool _button_selecting = false, _key_selecting = false;

	void render();
	void onMousemove(genv::event ev);
	void onButtonEvent(genv::event ev);
	void onKeyEvent(genv::event ev);
	void onShow();
	void onClose();

	int minHeight() const;
	int calcHeight(int n) const;
	void select(int index);
	inline int itemHeight() const;

public:
	DropDownPopup(DropDown* w);

	void draw(genv::canvas &out);
	bool isAbove(int x, int y) const;
};

#endif // WIDGETS_DrOPDOWN_HPP_INCLUDED
