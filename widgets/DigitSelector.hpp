#ifndef WIDGETS_DIGITSELECTOR_HPP_INCLUDED
#define WIDGETS_DIGITSELECTOR_HPP_INCLUDED

#include "../widgets.hpp"
#include "Button.hpp"
#include <functional>

class DigitSelectorPopup;

class DigitSelector : public Widget
{
protected:
	const int default_width = 30;
	const int default_height = 30;
	const Font default_font = Font("LiberationMono-Regular.ttf", 20);
	const genv::color default_color = COLOR_RED;
	const genv::color background_color = COLOR_BLACK;
	const int padding_size = 5;
	const int default_border_size = 1;
	const genv::color default_border_color = COLOR_RED;

	mycanvas _rendered_digitselector;
	mycanvas _rendered_digitselector_opened;
	int _width, _height;
	int _value = 0;
	Font _font = default_font;
	genv::color _textcolor = default_color;
	int _border_size = default_border_size;
	genv::color _border_color = default_border_color;
	bool _close_popup_on_release = false;
	DigitSelectorPopup* popup;
	friend class DigitSelectorPopup;
	std::function<void(int)> onchange_fun = 0;
	bool _enabled = true;
	bool _display_zero = true;

	void construct(int width, int height);
	void render();
	void showPopup();
	void closePopup();

	void onButtonEvent(genv::event ev);
	void onKeyEvent(genv::event ev);

public:
	DigitSelector();
	DigitSelector(Container* c);
	DigitSelector(bool display_zero);
	DigitSelector(Container* c, bool display_zero);
	DigitSelector(int width, int height);
	DigitSelector(Container* c, int width, int height);
	DigitSelector(int width, int height, bool display_zero);
	DigitSelector(Container* c, int width, int height, bool display_zero);

	void draw(genv::canvas &out);
	bool isAbove(int x, int y) const;
	inline bool isFocusable() const { return true; };
	bool canBePlacedHere(int x, int y) const;

	inline void setFont(std::string name, int size) { setFont(Font(name, size)); };
	void setFont(Font font);
	void setFontName(std::string name);
	void setFontSize(int size);

	void setColor(genv::color c);

	void setBorderSize(int size);
	void setBorderColor(genv::color c);

	int getWidth() const { return _width; };
	int getHeight() const { return _height; };

	int value() const { return _value; };
	void setValue(int value, bool trigger_event = false);

	void onChange(std::function<void(int)> fun);

	void setStatus(bool enabled);
	inline void enable() { setStatus(true); }
	inline void disable() { setStatus(false); }
};

class DigitSelectorPopup : public Popup
{
protected:
	DigitSelector* digitselector;
	friend void DigitSelector::showPopup();

	static bool is_initialized;
	static const int border_size = 1;
	static genv::color border_color;
	static const int button_size = 25;
	static Font font;
	static const int button_padding = 2;
	static genv::color background_color;

	static const int width = 2*border_size + 4*button_padding + 3*button_size;
	static const int height = 2*border_size + 5*button_padding + 4*button_size;

	static mycanvas rendered_popup;
	static Group* num_button_container;
	static std::vector<Button*> num_buttons;

	static void initialize();

	int _posx, _posy;
	bool _show_below = true;

	void render();
	void onButtonEvent(genv::event ev);
	void onKeyEvent(genv::event ev);
	void onShow();

public:
	DigitSelectorPopup(DigitSelector* w);

	void draw(genv::canvas &out);
	bool isAbove(int x, int y) const;
};

#endif // WIDGETS_DIGITSELECTOR_HPP_INCLUDED
