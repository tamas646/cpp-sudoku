#ifndef WIDGETS_LABEL_HPP_INCLUDED
#define WIDGETS_LABEL_HPP_INCLUDED

#include "../widgets.hpp"

class Label : public Widget
{
public:
	enum align { left, center, right };

protected:
	const int default_width = 200;
	const int default_height = 50;
	const std::string default_text = "";
	const Font default_font = Font("LiberationSans-Regular.ttf", 20);
	const genv::color default_color = COLOR_WHITE;
	const int default_border_size = 0;
	const genv::color default_border_color = COLOR_WHITE;
	const align default_text_align = left;

	mycanvas _rendered_label;
	int _width, _height;
	bool _fixed_size = true;
	std::string _text;
	Font _font = default_font;
	genv::color _textcolor = default_color;
	int _border_size = default_border_size;
	genv::color _border_color = default_border_color;
	align _text_align = default_text_align;

	void construct(std::string text);
	void construct(int width, int height, std::string text, align text_align);
	void render();

	void autoScale();

public:
	Label();
	Label(Container* c);
	Label(int width, int height);
	Label(Container* c, int width, int height);
	Label(std::string text);
	Label(Container* c, std::string text);
	Label(std::string text, int width, int height);
	Label(Container* c, std::string text, int width, int height);
	Label(std::string text, align text_align, int width, int height);
	Label(Container* c, std::string text, align text_align, int width, int height);

	void draw(genv::canvas &out);
	bool isAbove(int x, int y) const;
	virtual inline bool isFocusable() const { return false; }
	bool canBePlacedHere(int x, int y) const;

	inline void setFont(std::string name, int size) { setFont(Font(name, size)); };
	void setFont(Font font);
	void setFontName(std::string name);
	void setFontSize(int size);

	void setColor(genv::color c);

	void setBorder(int size, genv::color c);
	void setBorderSize(int size);
	void setBorderColor(genv::color c);
	int getWidth() const { return _width; };
	int getHeight() const { return _height; };

	void setValue(std::string text);
	std::string value() const;
};

#endif // WIDGETS_LABEL_HPP_INCLUDED
