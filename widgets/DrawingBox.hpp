#ifndef WIDGETS_DRAWINGBOX_HPP_INCLUDED
#define WIDGETS_DRAWINGBOX_HPP_INCLUDED

#include "../widgets.hpp"
#include <functional>

class DrawingBox : public Widget
{
protected:
	const int default_width = 100;
	const int default_height = 100;

	mycanvas _rendered_drawingbox;
	int _width, _height;
	std::function<void(mycanvas&)> _drawing_fun;

	void construct(int width, int height, std::function<void(mycanvas&)> drawing_fun);
	void render();

public:
	DrawingBox();
	DrawingBox(Container* c);
	DrawingBox(int width, int height);
	DrawingBox(Container* c, int width, int height);
	DrawingBox(int width, int height, std::function<void(mycanvas&)> drawing_fun);
	DrawingBox(Container* c, int width, int height, std::function<void(mycanvas&)> drawing_fun);

	void draw(genv::canvas &out);
	bool isAbove(int x, int y) const;
	inline bool isFocusable() const { return false; };
	bool canBePlacedHere(int x, int y) const;

	int getWidth() const { return _width; };
	int getHeight() const { return _height; };

	void onDraw(std::function<void(mycanvas&)> _drawing_fun);
};

#endif // WIDGETS_DRAWINGBOX_HPP_INCLUDED
