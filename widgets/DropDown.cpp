#include "DropDown.hpp"
#include "../window.hpp"

using namespace std;
using namespace genv;

/*--------  DropDown: protected  --------*/

void DropDown::render()
{
	_rendered_dropdown.clear();

	// border
	_rendered_dropdown << move_to(0, 0) << border_color << rectangle(_width, _height, border_size);

	// selected text
	if(!_list.empty())
	{
		string text = _font.shortenText(_list[_index], _width - 2*(border_size + padding_size + arrow_padding) - arrow_width);
		_rendered_dropdown << move_to(border_size + padding_size, border_size + padding_size) << text_color << genv::text(text);
	}

	// arrow
	int x = _width - border_size - arrow_padding - arrow_width;
	int y = (_height - arrow_height)/2;
	_rendered_dropdown << move_to(x, y) << arrow_color << line(arrow_width/2, arrow_height) << line(arrow_width/2, -arrow_height);
}

void DropDown::calcBoxSize()
{
	if(!_fixed_width) _width = calcBoxWidth();
	_height = 2*(border_size + padding_size) + _font.charHeight();
}
int DropDown::calcBoxWidth() const
{
	int sw_max = 0;
	for(int i = 0; i < int(_list.size()); ++i)
	{
		int sw = _font.stringWidth(_list[i]);
		if(sw > sw_max) sw_max = sw;
	}
	return 2*(border_size + padding_size + arrow_padding) + sw_max + arrow_width;
}

void DropDown::showPopup()
{
	Window* window = getWindow();
	if(!window) return;

	if(_list.empty()) return;
	int minh = popup->minHeight();
	int real_posy = getRealPosY();
	int available_height_below = window->getHeight() - real_posy - border_size - 1;
	int available_height_above = real_posy + _height - border_size;
	if(available_height_below >= available_height_above)
	{
		if(available_height_below >= minh)
		{
			popup->_show_below = true;
			popup->show();
		}
	}
	else
	{
		if(available_height_above >= minh)
		{
			popup->_show_below = false;
			popup->show();
		}
	}
}
void DropDown::closePopup()
{
	popup->close();
}

void DropDown::onButtonEvent(event ev)
{
	if(ev.button == btn_left)
	{
		if(popup->isOpen()) closePopup();
		else showPopup();
	}
}

void DropDown::onKeyEvent(event ev)
{
	if(ev.keycode == key_space || ev.keycode == key_enter)
	{
		showPopup();
	}
	else if(ev.keycode == key_up)
	{
		if(_index > 0) --_index;
	}
	else if(ev.keycode == key_down)
	{
		if(_index < int(_list.size()) - 1) ++_index;
	}
}

/*--------  DropDown: public  --------*/

DropDown::DropDown() { construct(default_list); }
DropDown::DropDown(Container* c) : Widget(c) { construct(default_list); }
DropDown::DropDown(vector<string> list) { construct(list); }
DropDown::DropDown(Container* c, vector<string> list) : Widget(c) { construct(list); }
// construct - protected
void DropDown::construct(vector<string> list)
{
	_list = list;
	_index = 0;
	calcBoxSize();
	popup = new DropDownPopup(this);
	_rendered_dropdown.open_with_font(_width, _height, _font);
	render();
}

void DropDown::draw(canvas &out)
{
	out << stamp(_rendered_dropdown, _posx, _posy);
}

bool DropDown::isAbove(int x, int y) const
{
	return x >= _posx && y >= _posy && x < _posx + _width && y < _posy + _height;
}

bool DropDown::canBePlacedHere(int x, int y) const
{
	return parent->getWidth() - x >= _width && parent->getHeight() - y >= _height;
}

void DropDown::setList(vector<string> list)
{
	_list = list;
	_index = _list.empty() ? -1 : 0;
	calcBoxSize();
	render();
}
void DropDown::setIndex(int index)
{
	if(index >= 0 && index < int(_list.size()))
	{
		_index = index;
		if(onchange_fun) onchange_fun(_index, _list[_index]);
		render();
	}
}

void DropDown::setBoxWidth(int width)
{
	if(_posx + width < parent->getWidth())
	{
		_fixed_width = true;
		_width = width;
		if(_width < min_width) _width = min_width;
		_rendered_dropdown.open(_width, _height);
		render();
	}
}

string DropDown::value() const
{
	return _list.empty() ? "" : _list[_index];
}
int DropDown::selectedIndex() const
{
	return _index;
}

void DropDown::clearItems()
{
	setList(vector<string>());
}
void DropDown::addItem(string item)
{
	if(_index == -1) _index = 0;
	_list.push_back(item);
	if(popup->isOpen())
	{
		popup->close();
	}
	render();
}
void DropDown::removeItem()
{
	if(_list.empty()) return;
	_list.erase(_list.begin()+_index);
	if(_index == int(_list.size()))
	{
		--_index;
	}
	if(popup->isOpen())
	{
		popup->close();
	}
	render();
}

void DropDown::onChange(function<void(int, string)> fun)
{
	onchange_fun = fun;
}

/*--------  DropDownPopup: protected  --------*/

mycanvas DropDownPopup::rendered_popup;

void DropDownPopup::render()
{
	rendered_popup.clear();
	// background
	rendered_popup << move_to(0, 0) << background_color << box(_width, _height);
	// border
	rendered_popup << move_to(0, 0) << border_color << rectangle(_width, _height, border_size);
	// items
	int item_width = _width - 2*border_size;
	int item_height = itemHeight();
	int x = border_size;
	int y = border_size;
	for(int i = _start_index; i < _start_index + _displayed_items && i < int(dropdown->_list.size()); ++i)
	{
		// item background
		if(i == dropdown->_index) rendered_popup << move_to(x, y) << selected_color << box(item_width, item_height);
		// item text
		string text = dropdown->_font.shortenText(dropdown->_list[i], item_width - 2*(item_border_size + item_padding_size));
		rendered_popup << move_to(x + item_border_size + item_padding_size, y + item_border_size + item_padding_size) << text_color << genv::text(text);

		y += item_height;
	}
}

void DropDownPopup::onMousemove(event ev)
{
	if(!_button_selecting && !_key_selecting)
	{
		if(ev.pos_x >= _posx + border_size && ev.pos_y >= _posy + border_size && ev.pos_x < _posx + _width - border_size && ev.pos_y < _posy + _height - border_size)
		{
			_focus_index = _start_index + (ev.pos_y - _posy - border_size) / itemHeight();
			render();
		}
	}
}

void DropDownPopup::onButtonEvent(event ev)
{
	if(ev.button == btn_left)
	{
		if(ev.pos_x >= _posx + border_size && ev.pos_y >= _posy + border_size && ev.pos_x < _posx + _width - border_size && ev.pos_y < _posy + _height - border_size)
		{
			select(_focus_index);
			_button_selecting = true;
			render();
		}
	}
	else if(ev.button == -btn_left)
	{
		if(_button_selecting) close();
	}
	else if(ev.button == btn_wheelup)
	{
		if(_start_index > 0)
		{
			--_start_index;
			render();
		}
	}
	else if(ev.button == btn_wheeldown)
	{
		if(_start_index < int(dropdown->_list.size()) - _displayed_items)
		{
			++_start_index;
			render();
		}
	}
}

void DropDownPopup::onKeyEvent(event ev)
{
	if(ev.keycode == key_up)
	{
		_focus_index = _keyboard_index;
		if(_focus_index > 0)
		{
			if(_focus_index <= _start_index)
			{
				_start_index = _focus_index - 1;
			}
			else if(_focus_index > _start_index + _displayed_items)
			{
				_start_index = _focus_index - _displayed_items;
			}
			--_focus_index;
			--_keyboard_index;
			render();
		}
	}
	else if(ev.keycode == key_down)
	{
		_focus_index = _keyboard_index;
		if(_focus_index < int(dropdown->_list.size() - 1))
		{
			if(_focus_index >= _start_index + _displayed_items - 1)
			{
				_start_index = _focus_index - _displayed_items + 2;
			}
			else if(_focus_index < _start_index - 1)
			{
				_start_index = _focus_index + 1;
			}
			++_focus_index;
			++_keyboard_index;
			render();
		}
	}
	else if(ev.keycode == key_home)
	{
		_start_index = 0;
		_focus_index = 0;
		_keyboard_index = 0;
		render();
	}
	else if(ev.keycode == key_end)
	{
		_start_index = dropdown->_list.size() - _displayed_items;
		_focus_index = dropdown->_list.size() - 1;
		_keyboard_index = dropdown->_list.size() - 1;
		render();
	}
	else if(ev.keycode == key_enter || ev.keycode == key_space)
	{
		select(_focus_index);
		_key_selecting = true;
		render();
	}
	else if(ev.keycode == -key_enter || ev.keycode == -key_space)
	{
		if(_key_selecting) close();
	}
}

void DropDownPopup::onShow()
{
	Window* window = dropdown->getWindow();
	int dropdown_real_posx = dropdown->getRealPosX();
	int dropdown_real_posy = dropdown->getRealPosY();
	int available_width = window->getWidth() - dropdown_real_posx - dropdown->border_size;
	_width = min(max(dropdown->calcBoxWidth() - 2*dropdown->border_size, dropdown->_width), available_width);
	int available_height;
	if(_show_below)
	{
		available_height = window->getHeight() - dropdown_real_posy - dropdown->border_size;
	}
	else
	{
		available_height = dropdown_real_posy + dropdown->border_size + dropdown->_height;
	}
	int displayable_items = (available_height - 2*border_size) / (2*(item_border_size + item_padding_size) + dropdown->_font.charHeight());
	_displayed_items = min(displayable_items, int(dropdown->_list.size()));
	_height = 2*border_size + _displayed_items*(2*(item_border_size + item_padding_size) + dropdown->_font.charHeight());

	_posx = dropdown_real_posx + dropdown->border_size;
	_posy = dropdown_real_posy + dropdown->border_size;
	if(!_show_below) _posy -= (_displayed_items-1)*itemHeight();

	_start_index = 0;
	if(dropdown->_index >= _displayed_items)
	{
		_start_index = dropdown->_index - _displayed_items;
	}
	_focus_index = dropdown->_index;
	_keyboard_index = _focus_index;
	rendered_popup.open_with_font(_width, _height, dropdown->_font);
	render();
}

void DropDownPopup::onClose()
{
	_button_selecting = false;
	_key_selecting = false;
}

int DropDownPopup::minHeight() const
{
	return calcHeight(min_displayed_items);
}
int DropDownPopup::calcHeight(int n) const
{
	return 2*border_size + n*(2*(item_border_size + item_padding_size) + dropdown->_font.charHeight());
}

void DropDownPopup::select(int index)
{
	dropdown->setIndex(index);
}

inline int DropDownPopup::itemHeight() const
{
	return 2*(item_border_size + item_padding_size) + dropdown->_font.charHeight();
}

/*--------  DropDownPopup: public  --------*/

DropDownPopup::DropDownPopup(DropDown* w) : Popup(w) { dropdown = w; }

void DropDownPopup::draw(canvas &out)
{
	out << stamp(rendered_popup, _posx, _posy);

	// hover item border
	int item_width = _width - 2*border_size;
	int item_height = itemHeight();
	out << move_to(_posx + border_size, _posy + border_size + (_focus_index - _start_index) * item_height) << selected_color << rectangle(item_width, item_height, item_border_size);
}

bool DropDownPopup::isAbove(int x, int y) const
{
	return x >= _posx && y >= _posy && x < _posx + _width && y < _posy + _height;
}
