#include "DrawingBox.hpp"
#include "../window.hpp"

using namespace genv;
using namespace std;

/*--------  protected  --------*/

void DrawingBox::render()
{
	_rendered_drawingbox.clear();
	if(_drawing_fun) _drawing_fun(_rendered_drawingbox);
}

/*--------  public  --------*/

DrawingBox::DrawingBox() { construct(default_width, default_height, 0); }
DrawingBox::DrawingBox(Container* c) : Widget(c) { construct(default_width, default_height, 0); }
DrawingBox::DrawingBox(int width, int height) { construct(width, height, 0); }
DrawingBox::DrawingBox(Container* c, int width, int height) : Widget(c) { construct(width, height, 0); }
DrawingBox::DrawingBox(int width, int height, function<void(mycanvas&)> drawing_fun) { construct(width, height, drawing_fun); }
DrawingBox::DrawingBox(Container* c, int width, int height, function<void(mycanvas&)> drawing_fun) : Widget(c) { construct(width, height, drawing_fun); }
// construct - protected
void DrawingBox::construct(int width, int height, function<void(mycanvas&)> drawing_fun)
{
	_width = width;
	_height = height;
	_drawing_fun = drawing_fun;
	_rendered_drawingbox.open(_width, _height);
	render();
}

void DrawingBox::draw(canvas &out)
{
	out << stamp(_rendered_drawingbox, _posx, _posy);
}

bool DrawingBox::isAbove(int x, int y) const
{
	return x >= _posx && y >= _posy && x < _posx + _width && y < _posy + _height;
}

bool DrawingBox::canBePlacedHere(int x, int y) const
{
	return parent->getWidth() - x >= _width && parent->getHeight() - y >= _height;
}

void DrawingBox::onDraw(function<void(mycanvas&)> drawing_fun)
{
	_drawing_fun = drawing_fun;
	render();
}
