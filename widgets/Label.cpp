#include "Label.hpp"
#include "../window.hpp"

using namespace std;
using namespace genv;

/*--------  protected  --------*/

void Label::render()
{
	_rendered_label.clear();
	if(_border_size > 0)
	{
		_rendered_label << move_to(0, 0) << _border_color << rectangle(_width, _height, _border_size);
	}
	if(_text_align == left) _rendered_label << move_to(_border_size, _border_size);
	else if(_text_align == center) _rendered_label << move_to((_width - _font.stringWidth(_text))/2, _border_size);
	else _rendered_label << move_to(_width - _border_size - _font.stringWidth(_text), _border_size);
	_rendered_label << _textcolor << text(_text);
}

void Label::autoScale()
{
	if(!_fixed_size)
	{
		_width = _font.stringWidth(_text);
		_height = _font.charHeight();
	}
}

/*--------  public  --------*/

Label::Label() { construct(default_width, default_height, default_text, default_text_align); }
Label::Label(Container* c) : Widget(c) { construct(default_width, default_height, default_text, default_text_align); }
Label::Label(int width, int height) { construct(width, height, default_text, default_text_align); }
Label::Label(Container* c, int width, int height) : Widget(c) { construct(width, height, default_text, default_text_align); }
Label::Label(string text) { construct(text); }
Label::Label(Container* c, string text) : Widget(c) { construct(text); }
Label::Label(string text, int width, int height) { construct(width, height, text, default_text_align); }
Label::Label(Container* c, string text, int width, int height) : Widget(c) { construct(width, height, text, default_text_align); }
Label::Label(string text, align text_align, int width, int height) { construct(width, height, text, text_align); }
Label::Label(Container* c, string text, align text_align, int width, int height) : Widget(c) { construct(width, height, text, text_align); }
// construct - protected
void Label::construct(string text) { _fixed_size = false; construct(_font.stringWidth(text), _font.charHeight(), text, default_text_align); }
void Label::construct(int width, int height, string text, align text_align)
{
	_width = width;
	_height = height;
	_text = text;
	_text_align = text_align;
	_rendered_label.open_with_font(_width, _height, _font);
	render();
}

void Label::draw(canvas &out)
{
	out << stamp(_rendered_label, _posx, _posy);
}

bool Label::isAbove(int x, int y) const
{
	return x >= _posx && y >= _posy && x < _posx + _width && y < _posy + _height;
}

bool Label::canBePlacedHere(int x, int y) const
{
	return parent->getWidth() - x >= _width && parent->getHeight() - y >= _height;
}

void Label::setFont(Font font)
{
	_font = font;
	_rendered_label.loadfont(_font);
	autoScale();
	render();
}
void Label::setFontName(string name)
{
	_font.name = name;
	_rendered_label.loadfont(_font);
	autoScale();
	render();
}
void Label::setFontSize(int size)
{
	_font.size = size;
	_rendered_label.loadfont(_font);
	autoScale();
	render();
}

void Label::setColor(color c)
{
	_textcolor = c;
	render();
}

void Label::setBorder(int size, color c)
{
	setBorderSize(size);
	setBorderColor(c);
	render();
}
void Label::setBorderSize(int size)
{
	_border_size = size;
	if(!_fixed_size)
	{
		_width += 2*size;
		_height += 2*size;
	}
	render();
}
void Label::setBorderColor(color c)
{
	_border_color = c;
	render();
}

void Label::setValue(string text)
{
	_text = text;
	autoScale();
	render();
}
string Label::value() const
{
	return _text;
}
