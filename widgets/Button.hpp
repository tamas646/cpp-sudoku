#ifndef WIDGETS_BUTTON_HPP_INCLUDED
#define WIDGETS_BUTTON_HPP_INCLUDED

#include "../widgets.hpp"
#include <functional>

class Button : public Widget
{
protected:
	const int default_width = 100;
	const int default_height = 50;
	const std::string default_text = "";
	const Font default_font = Font("LiberationSans-Regular.ttf", 20);
	const genv::color default_color = COLOR_RED;
	const genv::color background_color = COLOR_BLACK;
	const int padding_size = 5;
	const int border_size = 1;
	const genv::color border_color = COLOR_RED;

	mycanvas _rendered_button;
	mycanvas _rendered_button_pressed;
	int _width, _height;
	bool _fixed_size = true;
	std::string _text;
	Font _font = default_font;
	genv::color _textcolor = default_color;
	bool _button_selecting = false, _key_space_selecting = false, _key_enter_selecting = false;
	std::function<void()> _onpress_fun = [](){};
	std::function<void()> _onrelease_fun = [](){};

	void construct(std::string text);
	void construct(std::string text, int width, int height);
	void render();

	void autoScale();

	void onButtonEvent(genv::event ev);
	void onKeyEvent(genv::event ev);

public:
	Button();
	Button(Container* c);
	Button(std::string text);
	Button(Container* c, std::string text);
	Button(int width, int height);
	Button(Container* c, int width, int height);
	Button(std::string text, int width, int height);
	Button(Container* c, std::string text, int width, int height);

	void draw(genv::canvas &out);
	bool isAbove(int x, int y) const;
	inline bool isFocusable() const { return true; }
	bool canBePlacedHere(int x, int y) const;

	inline void setFont(std::string name, int size) { setFont(Font(name, size)); };
	void setFont(Font font);
	void setFontName(std::string name);
	void setFontSize(int size);

	void setColor(genv::color c);

	void setLabel(std::string text);

	int getWidth() const { return _width; };
	int getHeight() const { return _height; };

	void onPress(std::function<void()> fun);
	void onRelease(std::function<void()> fun);
};

#endif // WIDGETS_BUTTON_HPP_INCLUDED
