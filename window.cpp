#include "window.hpp"
#include "graphics.hpp"
#include "libpesta.hpp"
#include "widgets.hpp"

using namespace genv;

/*--------  public  --------*/

Window::Window(int w, int h) : Container(w, h)
{
	mygout.open(_width, _height);
}

void Window::showPopup(Popup *p)
{
	_popup = p;
	_popup->onShow();
}
void Window::closePopup()
{
	_popup->onClose();
	_popup = 0;
}
bool Window::isOpen(const Popup *p) const
{
	return _popup == p;
}

int Window::run()
{
	event ev;
	DisplayItem* hover_item = 0;
	DisplayItem* focus_item = 0;
	while(gin >> ev && _running)
	{
		if(ev.type == ev_mouse)
		{
			// handle hovered item
			bool noneHovered = true;
			if(_popup && _popup->isAbove(ev.pos_x, ev.pos_y))
			{
				noneHovered = false;
				if(_popup != hover_item)
				{
					if(hover_item) hover_item->mouseout(ev);
					hover_item = _popup;
					hover_item->mouseover(ev);
				}
				hover_item->mousemove(ev);
			}
			for(int i = widgets.size() - 1; i >= 0 && noneHovered; --i)
			{
				if(widgets[i]->isAbove(ev.pos_x, ev.pos_y))
				{
					noneHovered = false;
					if(widgets[i] != hover_item)
					{
						if(hover_item) hover_item->mouseout(ev);
						hover_item = widgets[i];
						hover_item->mouseover(ev);
					}
					hover_item->mousemove(ev);
				}
			}
			if(noneHovered)
			{
				if(hover_item) hover_item->mouseout(ev);
				hover_item = 0;
			}
			// handle focused widget
			if(ev.button == btn_left || ev.button == btn_right || ev.button == btn_middle)
			{
				if(hover_item)
				{
					DisplayItem* curr = hover_item->getFocusItem(ev.pos_x, ev.pos_y);
					if(_popup && curr != _popup && curr != _popup->parent()) _popup->close();
					if(curr != focus_item && !(_popup && _popup->parent() == focus_item))
					{
						if(focus_item) focus_item->blur();
						if(curr && curr == _popup && _popup->parent()->isFocusable())
						{
							focus_item = _popup->parent();
							focus_item->focus();
						}
						else if(curr && curr->isFocusable())
						{
							focus_item = curr;
							focus_item->focus();
						}
						else
						{
							focus_item = 0;
						}
					}
				}
				else
				{
					if(_popup) _popup->close();
					if(focus_item) focus_item->blur();
					focus_item = 0;
				}
			}
		}
		else if(ev.type == ev_key)
		{
			if(ev.keycode == key_escape)
			{
				if(_popup) _popup->close();
			}
		}
		// handle button and key events in the popup/focus_item (handle mouse move events at onMousemove())
		if(ev.type == ev_mouse && ev.button != 0)
		{
			if(_popup && focus_item == hover_item->getFocusItem(ev.pos_x, ev.pos_y)) focus_item->handleButtonEvent(ev);
			else if(_popup) _popup->handleButtonEvent(ev);
			else if(focus_item) focus_item->handleButtonEvent(ev);
		}
		else if(ev.type == ev_key)
		{
			if(_popup) _popup->handleKeyEvent(ev);
			else if(focus_item) focus_item->handleKeyEvent(ev);
		}
		onEvent(ev, focus_item);

		refresh();
	}
	return _exitcode;
}

void Window::refresh() const
{
	clear_window();
	for(Widget* a : widgets)
	{
		a->draw(gout);
	}
	if(_popup) _popup->draw(gout);
	gout << genv::refresh;
}

void Window::exit(int code)
{
	_exitcode = code;
	_running = false;
}
