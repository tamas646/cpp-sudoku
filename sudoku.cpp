#include "sudoku.hpp"
#include "libpesta.hpp"
#include <cstdio>
#include <iostream>
#include <fstream>

using namespace std;

/*--------  protected  --------*/

bool Sudoku::checkNumber(int i)
{
	int row_index = i/9;
	int column_index = i%9;
	int group_index = row_index/3*3 + column_index/3;
	bool found = false;

	// check in row
	for(int j = 0; j < 9; ++j)
	{
		if(compareNumbers(i, row_index*9 + j)) found = true;
	}

	// check in column
	for(int j = 0; j < 9; ++j)
	{
		if(compareNumbers(i, j*9 + column_index)) found = true;
	}

	// check in group
	for(int j = 0; j < 9; ++j)
	{
		if(compareNumbers(i, (group_index/3*3 + j/3)*9 + group_index%3*3 + j%3)) found = true;
	}

	if(found)
	{
		numbers[i].is_controversial = true;
		controversial_numbers.push_back(i);
	}
	return !found;
}
bool Sudoku::compareNumbers(int i, int j)
{
	if(numbers[j].value != 0 && !numbers[j].is_controversial && numbers[i].value == numbers[j].value && i != j)
	{
		numbers[j].is_controversial = true;
		controversial_numbers.push_back(j);
		return true;
	}
	return false;
}

bool Sudoku::loadBoardFromFile(string filename)
{
	ifstream fs(filename);
	if(!fs.good())
	{
		cout << "Error opening file: " << filename << endl;
		return false;
	}

	for(int i = 0; i < int(numbers.size()); ++i)
	{
		if(!fs.good())
		{
			cout << "Error reading from file: " << filename << endl;
			return false;
		}
		int a;
		fs >> a;
		if(!(a >= 0 && a <= 9))
		{
			cout << "Error reading from file: " << filename << endl;
			return false;
		}
		numbers[i].value = a;
		numbers[i].is_fixed = a != 0;
	}

	fs.close();

	return true;
}

void Sudoku::generateBoard(int n)
{
	// generate full board
	URandom r(9);
	// main diagonal
	for(int i = 0; i < 3; ++i)
	{
		for(int j = 0; j < 9; ++j)
		{
			int index = (i*3 + j/3)*9 + i*3 + j%3;
			numbers[index].value = r.next() + 1;
			numbers[index].is_fixed = true;
		}
		r.reset();
	}
	// rest
	fillBoard(0, 0);
	// for(int i = 0; i < 9; ++i)
	// {
	// 	// skip column
	// 	for(int j = 0; j < 6; ++j)
	// 	{
	// 		int row_index = i;
	// 		int column_index = ((i/3+1)*3+j)%9;
	// 		int group_index = row_index/3*3 + column_index/3;
	// 		// skip row
	// 		for(int k = 0; k < 9; ++k)
	// 		{
	// 			r.skip(numbers[row_index*9 + k].value - 1);
	// 		}
	// 		// skip column
	// 		for(int k = 0; k < 9; ++k)
	// 		{
	// 			r.skip(numbers[k*9 + column_index].value - 1);
	// 		}
	// 		// skip group
	// 		for(int k = 0; k <  + column_index%3; ++k)
	// 		{
	// 			r.skip(numbers[(group_index/3*3 + k/3)*9 + group_index%3*3 + k%3].value - 1);
	// 		}

	// 		int index = i*9 + column_index;
	// 		numbers[index].value = r.next() + 1;
	// 		numbers[index].is_fixed = true;
	// 		r.reset();
	// 	}
	// }

	// remove n fields
	// for(int i = 0; i < n; ++i)
	// {
	// 	// todo
	// }
}
bool Sudoku::fillBoard(int i, int j)
{
	if(i == 9) return true;

	URandom r(9);
	int row_index = i;
	int column_index = j;
	int group_index = row_index/3*3 + column_index/3;
	// skip row
	for(int k = 0; k < 9; ++k)
	{
		r.skip(numbers[row_index*9 + k].value - 1);
	}
	// skip column
	for(int k = 0; k < 9; ++k)
	{
		r.skip(numbers[k*9 + column_index].value - 1);
	}
	// skip group
	for(int k = 0; k <  + column_index%3; ++k)
	{
		r.skip(numbers[(group_index/3*3 + k/3)*9 + group_index%3*3 + k%3].value - 1);
	}

	int index = row_index*9 + column_index;
	do
	{
		numbers[index].value = r.next() + 1;
		if(numbers[index].value == 0) return false;
	}
	while(!fillBoard(i + j/9, j%9));
	numbers[index].is_fixed = true;
	return true;
}
bool Sudoku::isUnequivocal()
{
	// todo
}

void Sudoku::emptyBoard()
{
	for(int i = 0; i < int(numbers.size()); ++i)
	{
		numbers[i].value = 0;
		numbers[i].is_fixed = true;
	}
}

void Sudoku::clearControversialList()
{
	for(int i = 0; i < int(controversial_numbers.size()); ++i)
	{
		numbers[controversial_numbers[i]].is_controversial = false;
	}
	controversial_numbers.clear();
}

/*--------  public  --------*/

Sudoku::Sudoku() {}

void Sudoku::newGame(string filename)
{
	misstep_count = 0;
	emptyBoard();
	clearControversialList();

	if(!loadBoardFromFile(filename))
	{
		emptyBoard();
		return;
	}
}
void Sudoku::newGame(int difficulty)
{
	misstep_count = 0;
	emptyBoard();
	clearControversialList();

	generateBoard(difficulty);
}

void Sudoku::guessNumber(int i, int n)
{
	if(numbers[i].is_fixed || !(n >= 0 && n <= 9)) return;

	numbers[i].value = n;

	// refresh controversial numbers
	vector<int> temp = controversial_numbers;
	clearControversialList();
	for(int j = 0; j < int(temp.size()); ++j)
	{
		checkNumber(temp[j]);
	}

	// check new number for contradiction
	if(n != 0)
	{
		if(!checkNumber(i)) ++misstep_count;
	}
}

bool Sudoku::isFinished()
{
	for(int i = 0; i < int(numbers.size()); ++i)
	{
		if(numbers[i].value == 0 || numbers[i].is_controversial)
		{
			return false;
		}
	}
	return true;
}
